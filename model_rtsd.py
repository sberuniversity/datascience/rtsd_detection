import torch, torchvision
import torch.optim as optim
import collections
from torchvision.models import ResNet50_Weights
from torchvision.models.detection import RetinaNet_ResNet50_FPN_Weights
from torch.nn import functional as F
import cv2
import numpy as np
# import tarfile
import pandas as pd
from matplotlib import pyplot as plt
import torch
from torchvision import datasets
from torch.utils.data import DataLoader
import torch
from torch.utils.tensorboard import SummaryWriter
import os
from image_loader import DatasetRTSD

num_epoch = 20
cuda_device = 0
accumulation_steps = 4
batch_size = 1
switcher_dataset = 'full'    # 'mini' or 'full'
dataset_name = 'rtsd'
device = f'cuda:{cuda_device}' if cuda_device != -1 else 'cpu'

from GPUtil import showUtilization as gpu_usage
from numba import cuda
import torch

def free_gpu_cache():
    print("Initial GPU Usage")
    gpu_usage()                             

    torch.cuda.empty_cache()

    cuda.select_device(0)
    cuda.close()
    cuda.select_device(0)

    print("GPU Usage after emptying the cache")
    gpu_usage()


def unique_classes(data):
    uniques = set()
    for i, item in enumerate(data):
        # print(item[1])
        uniques.add(item[1])
    return len(uniques) + 1

dataser_custom = DatasetRTSD()
dataser_custom.get_info()

model = torchvision.models.detection.retinanet_resnet50_fpn(
    weights=None,
    progress=True,
    num_classes = unique_classes(dataser_custom.train_im_cl),
    weights_backbone = ResNet50_Weights.IMAGENET1K_V2,
    )
model = model.to(device)

optimizer = optim.Adam(model.parameters(), lr=1e-5)

scheduler = optim.lr_scheduler.ReduceLROnPlateau(optimizer, patience=3, verbose=True)

loss_hist = collections.deque(maxlen=500)

# def load_checkpoint(path, model, )

checkpoint_path = './checkpoints/rtsd-full_E-20_B-1_ACC_STEP_4/iter_530595.pt'
checkpoint = torch.load(checkpoint_path)
model.load_state_dict(checkpoint['model_state_dict'])
optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
epoch = checkpoint['epoch']
loss = checkpoint['loss']

def collate_fn(data):

    pics = []
    targets = []
    # labels = torch.zeros((len(data)), dtype=int).cuda()
    # boxes = torch.zeros((1, 4)).cuda()
    for i, item in enumerate(data):
        pic = torch.from_numpy(np.array(item[0], dtype=float)).cuda().float()
        shape = pic.shape
        pic /= 255
        pic = pic.reshape((shape[2], shape[0], shape[1]))
        pics.append(pic)
        box = torch.tensor([[0, 0, shape[1] - 1, shape[0] - 1]], dtype=float).cuda().float()
        target = {
                    'boxes': box,
                    'labels': torch.tensor([item[1]], dtype=int).cuda()
                }
        targets.append(target)

    return {
        'images': pics,
        'targets': targets
    }

train_data_loader = DataLoader(dataset=dataser_custom.train_im_cl,
                            batch_size=batch_size,
                            shuffle=True,
                            collate_fn=collate_fn,
                            drop_last=True,
                            )




def save_checkpoint(model, optimizer, path, epoch_num, dataset_count, iter_num, epoch_loss, epoch_end=False):
    file_checkpoint = path + f'/iter_{epoch_num * dataset_count + iter_num}_' + f'epoch_end_{epoch_num}.pt'
    torch.save({
        'epoch': epoch_num,
        'model_state_dict': model.state_dict(),
        'optimizer_state_dict': optimizer.state_dict(),
        'loss': epoch_loss,
    }, file_checkpoint)


model.train()

# if switcher_dataset == 'mini':
#     print('Num training images: {}'.format(len(data_loader_mini)))
#     dataset_count = len(data_loader_mini)
# else:
#     print('Num training images: {}'.format(len(dataset)))
#     dataset_count = len(dataset)
dataset_count = len(dataser_custom.train_im_cl)

folder_checkpoint = f'{dataset_name}-{switcher_dataset}_E-{num_epoch}_B-{batch_size}_ACC_STEP_{accumulation_steps}'
path = './checkpoints/' + folder_checkpoint
if not os.path.exists(path):
    os.mkdir(path)


writer = SummaryWriter(comment=f'_BATCH_{batch_size}_ACC_STEP_{accumulation_steps}_EPOCH_{num_epoch}_IMG_COUNT_{dataset_count}')

# if switcher_dataset == 'mini':
#     data_load = dataser_custom.train_im_cl
# else:
#     data_load = dataser_custom.train_im_cl

loss_train = np.inf

# optimizer.zero_grad()

# def train_model(num_epoch, optimizer, model, train_data_loader, writer, n):
#     if n < 0:
#         return

    # try:

for epoch_num in range(epoch, num_epoch):
    optimizer.zero_grad()

    model.train()
    # model.module.freeze_bn()

    epoch_loss = []
    

    for iter_num, data in enumerate(train_data_loader):
        # try:
        # optimizer.zero_grad()

        # print(data[1]) 

        if torch.cuda.is_available():
            losses = model(images=data['images'], targets=data['targets'])
        else:
            losses =  model(images=data['images'], targets=data['targets'])

            
        classification_loss = losses['classification'].mean() / accumulation_steps
        regression_loss = losses['bbox_regression'].mean() / accumulation_steps

        loss = classification_loss + regression_loss

        if bool(loss == 0):
            continue

        loss.backward()

        torch.nn.utils.clip_grad_norm_(model.parameters(), 0.1)

        if iter_num % accumulation_steps == 0:
            optimizer.step()
            optimizer.zero_grad()

        loss_hist.append(float(loss))

        epoch_loss.append(float(loss))

        print(
            'Epoch: {} | Iteration: {} | Classification loss: {:1.5f} | Regression loss: {:1.5f} | Running loss: {:1.5f}'.format(
                epoch_num, iter_num, float(classification_loss), float(regression_loss), np.mean(loss_hist)))
        # except Exception as e:
            # print(e)
            # continue
        if iter_num % 100 == 0:
            if epoch_loss[-1] < loss_train:
                loss_train = epoch_loss[-1]
                save_checkpoint(model=model,
                                optimizer=optimizer,
                                path=path,
                                epoch_num=epoch_num,
                                dataset_count=dataset_count,
                                iter_num=iter_num,
                                epoch_loss=loss
                                )
            writer.add_scalar("Loss_classification", classification_loss, epoch_num * dataset_count + iter_num)
            writer.add_scalar("Loss_regression", regression_loss, epoch_num * dataset_count + iter_num)
            writer.add_scalar("Loss_sum", loss, epoch_num * dataset_count + iter_num)
            for i in range(batch_size):
                writer.add_image(f'img_batch_{i}', data['images'][i], epoch_num * dataset_count + iter_num)
        if iter_num % 1000 == 0:
            save_checkpoint(model=model,
                            optimizer=optimizer,
                            path=path,
                            epoch_num=epoch_num,
                            dataset_count=dataset_count,
                            iter_num=iter_num,
                            epoch_loss=loss
                            )
    save_checkpoint(model=model,
    optimizer=optimizer,
    path=path,
    epoch_num=epoch_num, 
    dataset_count=dataset_count,
    iter_num=iter_num,
    epoch_loss=epoch_loss)

    # model.eval()
    # valid_loss = 0.0

    # for iter_num, data_test in enumerate(dataser_custom.test_im_cl):
    #     if torch.cuda.is_available():
    #         prediction = model(images=data_test['images'])
    #     else:
    #         prediction =  model(images=data_test['images'])
        
    #     classification_loss = losses['classification'].mean()
    #     regression_loss = losses['bbox_regression'].mean()

    #     loss = classification_loss + regression_loss

    #     if bool(loss == 0):
    #         continue

    #     loss.backward()

    #     torch.nn.utils.clip_grad_norm_(model.parameters(), 0.1)

    #     # if iter_num % 2 == 0:
    #     optimizer.step()

    #     loss_hist.append(float(loss))

    #     epoch_loss.append(float(loss))
# except RuntimeError as e:
#     free_gpu_cache()
#     train_model(num_epoch, optimizer, model, train_data_loader, writer, n-1)

        
writer.flush()
writer.close()

save_checkpoint(model=model,
                            optimizer=optimizer,
                            path=path,
                            epoch_num=epoch_num,
                            dataset_count=dataset_count,
                            iter_num=iter_num,
                            epoch_loss=loss
                            )