import torch, torchvision
import torch.optim as optim
import collections
from torchvision.models import ResNet50_Weights
from torchvision.models.detection import RetinaNet_ResNet50_FPN_Weights
from torch.nn import functional as F
import cv2
import numpy as np
# import tarfile
import pandas as pd
from matplotlib import pyplot as plt
import torch
from torchvision import datasets
from torch.utils.data import DataLoader
from tqdm import tqdm

num_epoch = 2
cuda_device = 0
batch_size = 2
device = f'cuda:{cuda_device}' if cuda_device != -1 else 'cpu'

dataset = datasets.GTSRB('./dataset_gtsrb/', download=True)

def unique_classes(data):
    uniques = set()
    for i, item in enumerate(data):
        # print(item[1])
        uniques.add(item[1])
    return len(uniques) + 1


model = torchvision.models.detection.retinanet_resnet50_fpn(
    weights=None,
    progress=True,
    num_classes = unique_classes(dataset),
    weights_backbone = ResNet50_Weights.IMAGENET1K_V2,
    )
model = model.to(device)

def find_max_dimensions(pics, maxd1, maxd2):
    for pic in pics:
        if pic.shape[0] > maxd1:
                maxd1 = pic.shape[0]
        if pic.shape[1] > maxd2:
            maxd2 = pic.shape[1]
    return maxd1, maxd2


def pad_dimensions(ten, maxd1, maxd2):
    pad1 = pad2 = pad3 = pad4 = pad5 = pad6 = 0
    dif1 = maxd1 - ten.shape[0]
    dif2 = maxd2 - ten.shape[1]
    if dif1 % 2 == 0:
        pad1 = dif1 // 2
        pad2 = dif1 // 2
    else:
        pad1 = (dif1 - 1) // 2
        pad2 = dif1 - (dif1 - 1) // 2
    if dif2 % 2 == 0:
        pad3 = dif2 // 2
        pad4 = dif2 // 2
    else:
        pad3 = (dif2 - 1) // 2
        pad4 = dif2 - (dif2 - 1) // 2
    pad = (pad5, pad6, pad3, pad4, pad1, pad2)
    # print(f'pad: {pad}')
    result = F.pad(ten, pad, 'constant', 0)
    return result


# def collate_fn(data):
#     pics = []
#     targets = []
#     for item in data:
#         pic = torch.from_numpy(np.array(item[0], dtype=np.float64))
#         pic /= 255
#         pics.append(pic)
#         targets.append(np.array(item[1]))
#     pics = np.array(pics, dtype=object)

#     maxd1, maxd2 = 0, 0
#     maxd1, maxd2 = find_max_dimensions(pics, maxd1, maxd2)
#     tensor = torch.zeros((len(data), maxd1, maxd2, 3))
#     for i, pic in enumerate(pics):
#         tensor[i] = pad_dimensions(pic, maxd1, maxd2)

#     return {
#         'img': tensor,
#         'annot': torch.from_numpy(np.array(targets))
#     }

def collate_fn(data):
    # count = 0
    # print(len(data))

    pics = []
    targets = []
    labels = torch.zeros((len(data)), dtype=int).cuda()
    boxes = torch.zeros((1, 4)).cuda()
    for i, item in enumerate(data):
        # if count > 1:
            # break
        pic = torch.from_numpy(np.array(item[0], dtype=float)).cuda().float()
        shape = pic.shape
        pic /= 255
        pic = pic.reshape((shape[2], shape[0], shape[1]))
        pics.append(pic)
        target = {
                    'boxes': torch.tensor([[0, 0, shape[1] - 1, shape[0] - 1]], dtype=float).cuda().float(),
                    'labels': torch.tensor([item[1]], dtype=int).cuda()
                }
        # labels[i] = item[1]
        # boxes[i] = torch.tensor([0, shape[1] - 1, 0, shape[0] - 1])
        targets.append(target)

        # count += 1
        # targets.append(np.array(item[1]))
    # pics = np.array(pics, dtype=object)
    # print(boxes.shape)
    # boxes_dict = {'boxes': boxes}
    # labels_dict = {'labels': labels}
    # targets_dict = {'boxes': boxes, 'labels': labels}
    # targets = [targets_dict]

    # maxd1, maxd2 = 0, 0
    # maxd1, maxd2 = find_max_dimensions(pics, maxd1, maxd2)
    # tensor = torch.zeros((len(data), maxd1, maxd2, 3))
    # for i, pic in enumerate(pics):
        # tensor[i] = pad_dimensions(pic, maxd1, maxd2)

    return {
        'images': pics,
        'targets': targets
    }


data_loader = DataLoader(dataset=dataset,
                            batch_size=4,
                            shuffle=True,
                            collate_fn=collate_fn,
                            drop_last=True,
                            )


optimizer = optim.Adam(model.parameters(), lr=1e-5)

scheduler = optim.lr_scheduler.ReduceLROnPlateau(optimizer, patience=3, verbose=True)

loss_hist = collections.deque(maxlen=500)

model.train()
# model.module.freeze_bn()

# data_loader_mini = []
# count = 0
# for data in data_loader:
#     if count > 2:
#         break
#     data_loader_mini.append(data)
#     count += 1


print('Num training images: {}'.format(len(dataset)))

for epoch_num in range(2):

    model.train()
    # model.module.freeze_bn()

    epoch_loss = []

    for iter_num, data in enumerate(data_loader):
        # try:
        optimizer.zero_grad()

        if torch.cuda.is_available():
            losses = model(images=data['images'], targets=data['targets'])
        else:
            losses =  model(images=data['images'], targets=data['targets'])
            
        classification_loss = losses['classification'].mean()
        regression_loss = losses['bbox_regression'].mean()

        loss = classification_loss + regression_loss

        if bool(loss == 0):
            continue

        loss.backward()

        torch.nn.utils.clip_grad_norm_(model.parameters(), 0.1)

        optimizer.step()

        loss_hist.append(float(loss))

        epoch_loss.append(float(loss))

        print(
            'Epoch: {} | Iteration: {} | Classification loss: {:1.5f} | Regression loss: {:1.5f} | Running loss: {:1.5f}'.format(
                epoch_num, iter_num, float(classification_loss), float(regression_loss), np.mean(loss_hist)))
        # except Exception as e:
            # print(e)
            # continue

# Print model's state_dict
print("Model's state_dict:")
for param_tensor in model.state_dict():
    print(param_tensor, "\t", model.state_dict()[param_tensor].size())

# Print optimizer's state_dict
print("Optimizer's state_dict:")
for var_name in optimizer.state_dict():
    print(var_name, "\t", optimizer.state_dict()[var_name])


torch.save(model.state_dict(), './weights/gtsrb_weights.pt')