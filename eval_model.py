import torch, torchvision
import torch.optim as optim
import collections
from torchvision.models import ResNet50_Weights
from torchvision.models.detection import RetinaNet_ResNet50_FPN_Weights
from torch.nn import functional as F
import cv2
import numpy as np
# import tarfile
import pandas as pd
from matplotlib import pyplot as plt
import torch
from torchvision import datasets
from torch.utils.data import DataLoader
import torch
from torch.utils.tensorboard import SummaryWriter
import os



from image_loader import DatasetRTSD


def unique_classes(data):
    uniques = set()
    for i, item in enumerate(data):
        # print(item[1])
        uniques.add(item[1])
    return len(uniques) + 1



dataser_custom = DatasetRTSD()
dataser_custom.get_info()

model = torchvision.models.detection.retinanet_resnet50_fpn(
    weights=None,
    progress=True,
    num_classes = unique_classes(dataser_custom.train_im_cl),
    weights_backbone = ResNet50_Weights.IMAGENET1K_V2,
    )
model = model.to('cpu')

optimizer = optim.Adam(model.parameters(), lr=1e-5)

# scheduler = optim.lr_scheduler.ReduceLROnPlateau(optimizer, patience=3, verbose=True)

loss_hist = collections.deque(maxlen=500)

# model.train()


# model = Net()
# optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)
checkpoint_path = './checkpoints/rtsd-full_E-20_B-1_ACC_STEP_4/iter_530595.pt'
checkpoint = torch.load(checkpoint_path)
model.load_state_dict(checkpoint['model_state_dict'])
optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
epoch = checkpoint['epoch']
loss = checkpoint['loss']


def collate_fn(data):

    pics = []
    targets = []
    labels = torch.zeros((len(data)), dtype=int).cuda()
    boxes = torch.zeros((1, 4)).cuda()
    for i, item in enumerate(data):
        pic = torch.from_numpy(np.array(item[0], dtype=float)).cuda().float()
        shape = pic.shape
        pic /= 255
        pic = pic.reshape((shape[2], shape[0], shape[1]))
        pics.append(pic)
        box = torch.tensor([[0, 0, shape[1] - 1, shape[0] - 1]], dtype=float).cuda().float()
        target = {
                    'boxes': box,
                    'labels': torch.tensor([item[1]], dtype=int).cuda()
                }
        targets.append(target)

    return {
        'images': pics,
        'targets': targets
    }

test_data_loader = DataLoader(dataset=dataser_custom.test_im_cl,
                            batch_size=1,
                            shuffle=False,
                            collate_fn=collate_fn,
                            drop_last=True,
                            )

index = 200

pic = torch.from_numpy(np.array(dataser_custom.test_im_cl[index][0], dtype=float)).float()
pic /= 255
shape = pic.shape
pic = pic.reshape((shape[2], shape[0], shape[1]))
pic.shape
box = torch.tensor([[0, 0, shape[1] - 1, shape[0] - 1]], dtype=float).float()
target = {
                    'boxes': box,
                    'labels': torch.tensor([dataser_custom.test_im_cl[index][1]], dtype=int)
                }

model.eval()
with torch.no_grad():
    # for iter, data in test_data_loader:
        # print(data)
    print(model([pic], [target]))
    print(target)
    # print(model(dataser_custom.test_im_cl[0][0]))