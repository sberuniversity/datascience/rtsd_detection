import numpy as np
import tarfile
import pandas as pd
# from matplotlib import pyplot as plt
import torch
from torchvision import datasets
from torch.utils.data import DataLoader
from torch.utils.data import IterableDataset
from tqdm import tqdm
from PIL import Image


class CustomDataIterator(IterableDataset):
    def __init__(self, data) -> None:
        super().__init__()
        self.data = data

    def __iter__(self):
        return iter(self.data)

    def __getitem__(self, index):
        return self.data[index]

    def __len__(self):
        return len(self.data)


class DatasetRTSD:
    def __init__(self) -> None:
        self.train_im_cl = None
        self.test_im_cl = None
        self.full_img_cl = None
        # self.map_class_names = {"2_1": 1, "1_23": 2, "1_17": 3, "3_24": 4, "8_2_1": 5, "5_20": 6, "5_19_1": 7, "5_16": 8, "3_25": 9, "6_16": 10, "7_15": 11, "2_2": 12, "2_4": 13, "8_13_1": 14, "4_2_1": 15, "1_20_3": 16, "1_25": 17, "3_4": 18, "8_3_2": 19, "3_4_1": 20, "4_1_6": 21, "4_2_3": 22, "4_1_1": 23, "1_33": 24, "5_15_5": 25, "3_27": 26, "1_15": 27, "4_1_2_1": 28, "6_3_1": 29, "8_1_1": 30, "6_7": 31, "5_15_3": 32, "7_3": 33, "1_19": 34, "6_4": 35, "8_1_4": 36, "8_8": 37, "1_16": 38, "1_11_1": 39, "6_6": 40, "5_15_1": 41, "7_2": 42, "5_15_2": 43, "7_12": 44, "3_18": 45, "5_6": 46, "5_5": 47, "7_4": 48, "4_1_2": 49, "8_2_2": 50, "7_11": 51, "1_22": 52, "1_27": 53, "2_3_2": 54, "5_15_2_2": 55, "1_8": 56, "3_13": 57, "2_3": 58, "8_3_3": 59, "2_3_3": 60, "7_7": 61, "1_11": 62, "8_13": 63, "1_12_2": 64, "1_20": 65, "1_12": 66, "3_32": 67, "2_5": 68, "3_1": 69, "4_8_2": 70, "3_20": 71, "3_2": 72, "2_3_6": 73, "5_22": 74, "5_18": 75, "2_3_5": 76, "7_5": 77, "8_4_1": 78, "3_14": 79, "1_2": 80, "1_20_2": 81, "4_1_4": 82, "7_6": 83, "8_1_3": 84, "8_3_1": 85, "4_3": 86, "4_1_5": 87, "8_2_3": 88, "8_2_4": 89, "1_31": 90, "3_10": 91, "4_2_2": 92, "7_1": 93, "3_28": 94, "4_1_3": 95, "5_4": 96, "5_3": 97, "6_8_2": 98, "3_31": 99, "6_2": 100, "1_21": 101, "3_21": 102, "1_13": 103, "1_14": 104, "2_3_4": 105, "4_8_3": 106, "6_15_2": 107, "2_6": 108, "3_18_2": 109, "4_1_2_2": 110, "1_7": 111, "3_19": 112, "1_18": 113, "2_7": 114, "8_5_4": 115, "5_15_7": 116, "5_14": 117, "5_21": 118, "1_1": 119, "6_15_1": 120, "8_6_4": 121, "8_15": 122, "4_5": 123, "3_11": 124, "8_18": 125, "8_4_4": 126, "3_30": 127, "5_7_1": 128, "5_7_2": 129, "1_5": 130, "3_29": 131, "6_15_3": 132, "5_12": 133, "3_16": 134, "1_30": 135, "5_11": 136, "1_6": 137, "8_6_2": 138, "6_8_3": 139, "3_12": 140, "3_33": 141, "8_4_3": 142, "5_8": 143, "8_14": 144, "8_17": 145, "3_6": 146, "1_26": 147, "8_5_2": 148, "6_8_1": 149, "5_17": 150, "1_10": 151, "8_16": 152, "7_18": 153, "7_14": 154, "8_23": 155}

    @staticmethod
    def _extract_df(filepath):
        return pd.read_csv(filepath)

    def _get_images_classes(self, path_to_df, filepath):
        df = self._extract_df(path_to_df)
        dataset = []
        # for index in range(2):
        for index in range(len(df)):
            path = filepath + df['filename'].iloc[index]
            img = Image.open(path)
            label = df['class_number'].iloc[index]
            dataset.append((img, label))
            # img.close()
        return dataset

    def _get_im_cl_from_diff_df(self, paths_to_df: list, paths: list):
        dataset_common = []
        for i in range(len(paths)):
            dataset_common.extend(self._get_images_classes(path_to_df=paths_to_df[i], filepath=paths[i]))
        return dataset_common
    
    def get_info(self):
        filepath_to_df = [
            './dataset2/rtsd-public/extracted/rtsd-r1/gt_train.csv',
            './dataset2/rtsd-public/extracted/rtsd-r3/gt_train.csv',
            './dataset2/rtsd-public/extracted/rtsd-r1/gt_test.csv',
            './dataset2/rtsd-public/extracted/rtsd-r3/gt_test.csv',
            # './dataset2/rtsd-public/full-gt.csv',
        ]

        train_data = self._get_im_cl_from_diff_df(
            paths_to_df=filepath_to_df[:2], 
            paths=['./dataset2/rtsd-public/extracted/rtsd-r1/train/', 
            './dataset2/rtsd-public/extracted/rtsd-r3/train/'])

        self.train_im_cl = train_data

        test_data = self._get_im_cl_from_diff_df(
            paths_to_df=filepath_to_df[2:4], 
            paths=['./dataset2/rtsd-public/extracted/rtsd-r1/test/', 
            './dataset2/rtsd-public/extracted/rtsd-r3/test/'])
        
        self.test_im_cl = test_data

        df = pd.read_csv('./reduced_full_gt.csv')
        self.full_img_cl = self.form_dataset(df, './dataset_reduced/')
        
        # self.full_img_cl = self._get_im_cl_from_diff_df(
        #     paths_to_df=filepath_to_df[-1], 
        #     paths=['./dataset2/rtsd-public/extracted/rtsd-frames/'])

    @staticmethod
    def form_dataset(df, path):
        data = []
        number_of_classes = len(df['sign_class'].unique())
        it = iter(np.arange(number_of_classes))
        mapping_classes = {}
        for x in df['sign_class'].unique():
            mapping_classes.update({x: next(it)})
        df['classes_number'] = list(map(lambda x: mapping_classes[x], df['sign_class']))

        for i in range(len(df)):
            filename = path + df['filename'].iloc[i]
            pic = Image.open(filename)
            # pic = np.array(pic) / 255
            class_num = df['classes_number'].iloc[i]
            x1, y1, x2, y2 = df['x1'], df['y1'], df['x2'], df['y2']
            # x1, y1, x2, y2 = get_coords_for_cut(df, i)
            res = [pic, class_num, x1, y1, x2, y2]
            data.append(res)
        return data
    
        